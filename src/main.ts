import { User } from './users/entities/user.entity';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(3000);

  const admin = new User()
  admin.name="user1"
  admin.password="u1"
  await admin.save()
}
bootstrap();
