import { Company } from './../../companies/entities/company.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany, BaseEntity
} from 'typeorm';

@Entity()
export class User extends BaseEntity{
  @PrimaryGeneratedColumn() id: number;

  @Column() name: string;

  @Column() password: string;

  @OneToMany(
    () => Company,
    (c: Company) => c.user,
  )
  companies: Company[];
}
