import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { TypeOrmModuleAsyncOptions, TypeOrmModuleOptions, TypeOrmOptionsFactory } from '@nestjs/typeorm'
import { TypeOrmConfigs } from "./typeOrm.config.def"

@Injectable()
export class TypeOrmConfig implements TypeOrmOptionsFactory {
    constructor(private readonly configService: ConfigService) {
    }

    async createTypeOrmOptions(): Promise<TypeOrmModuleOptions> {

        const r = (TypeOrmConfigs.find(
            (cfg: TypeOrmModuleAsyncOptions) =>{
                return cfg.name === this.configService.get('NODE_ENV')
            }
                ,
            ) ||
            TypeOrmConfigs.find(
                (cfg: TypeOrmModuleAsyncOptions) => cfg.name === 'default',
            )) as TypeOrmModuleOptions

        const res = JSON.parse(JSON.stringify(r))
        res.name = undefined
        res.autoLoadEntities = true
        return res
    }
}
  