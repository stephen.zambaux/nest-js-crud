export const TypeOrmConfigs = [
    {
        name: "default",
        type: 'sqlite',
        host: 'localhost',
        port: 5432,
        logging: "all",
        database: 'nest-crud',
        entities: [__dirname + '/../**/*.entity.{js,ts}'],
        synchronize: true
    },
]
