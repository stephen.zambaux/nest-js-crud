import { Controller, UseGuards, UseInterceptors } from '@nestjs/common';
import {
  Crud,
  CrudAuth,
  CrudController,
  CrudRequest,
  CrudRequestInterceptor,
  Override,
  ParsedRequest,
} from '@nestjsx/crud';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { User } from 'src/users/entities/user.entity';
import { CompaniesService } from './companies.service';
import { Company } from './entities/company.entity';

@Crud({
  model: {
    type: Company,
  },
  query: {
    join: {
      user: {
        eager: true,
        select: false,
      },
    },
  },
})
//@UseInterceptors(AuthInterceptor, CrudRequestInterceptor)
@UseInterceptors(CrudRequestInterceptor)
@CrudAuth({
  property: 'user',
  filter: (user: User) => ({
    'user.id': user.id,
  }),
  persist: (user: User) => ({
    user,
  }),
})
@UseGuards(JwtAuthGuard)
@Controller('companies')
export class CompaniesController implements CrudController<Company> {
  constructor(public service: CompaniesService) {}

  get base(): CrudController<Company> {
    return this;
  }

  @Override()
  getMany(@ParsedRequest() req: CrudRequest) {
    return this.base.getManyBase(req);
  }
}
