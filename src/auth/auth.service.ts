import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { getManager } from 'typeorm';
import { User } from 'src/users/entities/user.entity';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService
  ) {}

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await getManager().getRepository(User).findOne({where:{name:username}})
    if (user && user.password === pass) {
        const { password: pass, ...result } = user;
        return result;
    }
    return null;
  }

  async login(user: User) {
    const payload = { name: user.name, id: user.id };
    return {
        payload,
      access_token: this.jwtService.sign({ name: payload.name, id: payload.id}),
    };
  }
}