import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable } from 'rxjs';

@Injectable()
export class AuthInterceptor<T, R> implements NestInterceptor<T, R> {
  // eslint-disable-next-line class-methods-use-this
  public intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<R> {
    const req = context.switchToHttp().getRequest();

    if (req.method === 'POST') {
      if (Array.isArray(req.body.bulk)) {
        for (const el of req.body.bulk) {
          el.user = { id: req.user.id };
        }
      } else {
        // Works for createOneBase route.
        req.body.user = { id: req.user.id };
      }
    } else {
      // Works for getManyBase, getOneBase, updateOneBase, replaceOneBase, deleteOneBase routes.
      req.query.s = JSON.stringify({
        ...JSON.parse(req.query.s),
        'user.id': req.user.id,
      });
    }

    return next.handle();
  }
}
